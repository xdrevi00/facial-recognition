import sys
from keras.models import Model
from keras.layers import Dense, Dropout, Flatten, Activation, Input
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import BatchNormalization
from keras import regularizers

def basic(model_input):
    net = model_input

    net = Conv2D(32, (3, 3), padding='same') (net)
    net = BatchNormalization() (net)
    net = Activation('relu') (net)

    net = Conv2D(32, (3, 3), padding='same') (net)
    net = BatchNormalization() (net)
    net = Activation('relu') (net)

    net = MaxPooling2D(pool_size=(2, 2)) (net)

    net = Conv2D(64, (3, 3), padding='same') (net)
    net = BatchNormalization() (net)
    net = Activation('relu') (net)
    net = Conv2D(64, (3, 3), padding='same') (net)
    net = BatchNormalization() (net)
    net = Activation('relu') (net)
    net = MaxPooling2D(pool_size=(2, 2)) (net)

    net = Flatten() (net)
    net = Dense(256) (net)
    net = BatchNormalization() (net)
    net = Activation('relu') (net)
    net = Dropout(0.5) (net)

    return net

def vgg_16(model_input):
    net = model_input
    
    # 2 x 64
    net = Conv2D(64, (3, 3), padding='same') (net)
    net = BatchNormalization() (net)
    net = Activation('relu') (net)

    net = Conv2D(64, (3, 3), padding='same') (net)
    net = BatchNormalization() (net)
    net = Activation('relu') (net)

    # POOL
    net = MaxPooling2D(pool_size=(2, 2), strides=(2, 2)) (net)

    # 2 x 128
    net = Conv2D(128, (3, 3), padding='same') (net)
    net = BatchNormalization() (net)
    net = Activation('relu') (net)

    net = Conv2D(128, (3, 3), padding='same') (net)
    net = BatchNormalization() (net)
    net = Activation('relu') (net)

    # POOL
    net = MaxPooling2D(pool_size=(2, 2), strides=(2, 2)) (net)

    # 2 x 256
    net = Conv2D(256, (3, 3), padding='same') (net)
    net = BatchNormalization() (net)
    net = Activation('relu') (net)

    net = Conv2D(256, (3, 3), padding='same') (net)
    net = BatchNormalization() (net)
    net = Activation('relu') (net)

    # POOL
    net = MaxPooling2D(pool_size=(2, 2), strides=(2, 2)) (net)

    # 3 x 512
    net = Conv2D(512, (3, 3), padding='same') (net)
    net = BatchNormalization() (net)
    net = Activation('relu') (net)

    net = Conv2D(512, (3, 3), padding='same') (net)
    net = BatchNormalization() (net)
    net = Activation('relu') (net)

    # POOL
    net = MaxPooling2D(pool_size=(2, 2), strides=(2, 2)) (net)

    # 3 x 512
    net = Conv2D(512, (3, 3), padding='same') (net)
    net = BatchNormalization() (net)
    net = Activation('relu') (net)

    net = Conv2D(512, (3, 3), padding='same') (net)
    net = BatchNormalization() (net)
    net = Activation('relu') (net)

    # POOL
    net = MaxPooling2D(pool_size=(2, 2), strides=(2, 2)) (net)

    net = Flatten() (net)
    net = Dense(4096) (net)
    net = BatchNormalization() (net)
    net = Activation('relu') (net)
    net = Dropout(0.5) (net)

    return net

def get_model(model_name, model_input):
    if model_name == 'vgg_16':
        return vgg_16(model_input)
    elif model_name == 'basic':
        return basic(model_input)
    else:
        sys.stderr.write('Unknown model requested: {}\n'.format(model_name))
        sys.exit(1)
