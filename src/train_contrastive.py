import os
import time
import argparse
import numpy as np
import matplotlib.pyplot as plt
import keras
from keras import backend as K
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Flatten, Activation, Lambda
from keras.layers import Input, Reshape, Dense, Dropout, Flatten, BatchNormalization
from keras.optimizers import SGD, Adam
from keras.callbacks import ReduceLROnPlateau
import train_common as tc
from train_common import PairDataset, load_keras_model, euclidean_distance, eucl_dist_output_shape, contrastive_loss, accuracy_for_contrastive_loss, l2_normalize

from models import models

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('dataset_dir', type=str, help="Dataset directory containing images folder and labels.txt file.")
    parser.add_argument('save_model_dir', type=str, help="""Directory name where the model and training loss history will be saved. Data is saved to 
                                                    project_root/models/trained/out_model_dir directory.""")
    parser.add_argument('--image_dim', type=int, default=96, help="Dimensions of the aligned images in the dataset. Width and height should be the same.")
    parser.add_argument('--batch_size', type=int, default=32, help='Batch size.')
    parser.add_argument('--embedding_size', type=int, default=256, help='Embedding size.')
    parser.add_argument('--learn_rate', type=float, default=0.001, help='Initial learning rate.')
    parser.add_argument('--optim', type=str, default='sgd', help='Optimizer. Can be adam|sgd')

    subparsers = parser.add_subparsers(dest='mode', help="Mode.")
    subparsers.required = True
    
    from_scratch_parser = subparsers.add_parser('from_scratch', help="Train a new model from scratch.")
    from_scratch_parser.add_argument('--model_arch', type=str, default='vgg_16', help='Model architecture to use. Can be vgg_16|basic.')

    from_pretrained_parser = subparsers.add_parser('from_pretrained', help="Continue training an pretrained model.")
    from_pretrained_parser.add_argument('pretrained_model_dir', type=str, help='Directory containing the pretrained model.')
    from_pretrained_parser.add_argument('--freeze_pretrained', default=False, action='store_true', help='Whether to freeze pretrained layers during training.')

    args = parser.parse_args()
    return vars(args)

# Run like this for example: python src/train.py overfit/ basic_model.
if __name__ == '__main__':
    args = parse_args()
    
    img_width, img_height = args['image_dim'], args['image_dim']
    input_shape = (img_height, img_width, 3)    # Tensorflow backend

    # Model producing embedding vector for a single image
    if args['mode'] == 'from_scratch':
        input_data = Input(shape=input_shape)
        embed_net = models.get_model(args['model_arch'], input_data)
    elif args['mode'] == 'from_pretrained':
        pretrained = load_keras_model(args['pretrained_model_dir'])
        
        ''' For softmax functional
        pretrained.layers.pop()  # Pop softmax layer
        input_data = pretrained.layers[0].output
        pretrained = Model(input_data, pretrained.layers[-1].output)
        pretrained.summary()
        embed_net = pretrained.output
        '''
        '''
        # For softmax sequential
        pretrained.pop()
        pretrained.build()
        input_data = Input(shape=input_shape)
        embed_net = pretrained.model (input_data)
        '''
        if args['freeze_pretrained']:
            for layer in pretrained.layers:
                layer.trainable = False
        else:
            for layer in pretrained.layers[-2].layers[1].layers:
                layer.trainable = True
        model = pretrained
        
    '''
    embed_net = Dense(args['embedding_size'], name='embedding') (embed_net)
    embed_net = Lambda(l2_normalize, output_shape=[args['embedding_size']], name='l2_normalize') (embed_net)
    embed_model = Model(inputs=input_data, outputs=embed_net, name='embed_model')
    embed_model.summary()

    # Model computing L2 distance between the embedding vectors of two images
    input_a = Input(shape=input_shape, name='image_1')
    input_b = Input(shape=input_shape, name='image_2')
    net_a = embed_model(input_a)
    net_b = embed_model(input_b)
    distance = Lambda(euclidean_distance, output_shape=eucl_dist_output_shape, name='l2_distance')([net_a, net_b])
    model = Model([input_a, input_b], distance)
    '''
    model.summary()

    # Training parameters 
    initial_lr = args['learn_rate']
    if args['optim'] == 'adam':
        opt = Adam(lr=initial_lr)
    elif args['optim'] == 'sgd':
        opt = SGD(lr=initial_lr, momentum=0.9, nesterov=True)
    model.compile(loss=contrastive_loss, optimizer=opt, metrics=[accuracy_for_contrastive_loss])
    training_cb = tc.TrainingCallback(model, args['save_model_dir'], existing_model_dir=None, accuracy_function='accuracy_for_contrastive_loss')
    reduce_lr_cb = ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=4, min_lr=0.001*initial_lr)

    # Data
    train_data = PairDataset(os.path.join(args['dataset_dir'], 'images', 'train'))
    val_data = PairDataset(os.path.join(args['dataset_dir'], 'images', 'val'))
    val_pos_pair_count = 10000
    val_neg_pair_count = 10000
    val_input_net_a, val_input_net_b, val_labels = val_data.create_data(val_pos_pair_count, val_neg_pair_count)

    # Training
    epochs = 10000
    for i in range(epochs):
        tr_pos_pair_count = 25000
        tr_neg_pair_count = 25000
        tr_input_net_a, tr_input_net_b, tr_labels = train_data.create_data(tr_pos_pair_count, tr_neg_pair_count)

        model.fit(
            [tr_input_net_a, tr_input_net_b], 
            y=tr_labels, 
            batch_size=args['batch_size'],
            epochs=i + 1,
            verbose=1,
            validation_data=([val_input_net_a, val_input_net_b], val_labels),
            initial_epoch=i,
            callbacks=[training_cb, reduce_lr_cb])
