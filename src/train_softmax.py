import os
import shutil
import time
import argparse
import numpy as np
import keras
from keras import backend as K
from keras.preprocessing.image import ImageDataGenerator
from keras.layers import Dense, Dropout, Flatten, Activation, Input
from keras.models import Sequential, Model
from keras.optimizers import SGD, Adam
from keras.models import load_model

from models import models
import train_common

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('dataset_dir', type=str, help="Dataset directory containing images folder and labels.txt file.")
    parser.add_argument('save_model_dir', type=str, help="""Directory name where the model and training loss history will be saved. Data is saved to 
                                                        project_root/models/trained/out_model_dir directory.""")
    parser.add_argument('--image_dim', type=int, default=96, help="Dimensions of the aligned images in the dataset. Width and height should be the same.")
    parser.add_argument('--batch_size', type=int, default=64, help='Batch size.')

    subparsers = parser.add_subparsers(dest='mode', help="Mode.")
    subparsers.required = True
    
    from_scratch_parser = subparsers.add_parser('from_scratch', help="Train a new model from scratch.")
    from_scratch_parser.add_argument('model_arch', type=str, help='Model architecture to use. Can be [vgg_16|basic]')

    from_pretrained_parser = subparsers.add_parser('from_pretrained', help="Continue training a pretrained model.")
    from_pretrained_parser.add_argument('pretrained_model_dir', type=str, help='Directory containing the pretrained model.')

    args = parser.parse_args()
    return vars(args)

# Run like this for example: python src/train.py overfit/ basic_model.
if __name__ == '__main__':
    args = parse_args()
    
    img_width, img_height = args['image_dim'], args['image_dim']
    if K.image_data_format() == 'channels_first':
        input_shape = (3, img_width, img_height)    # Theano backend
    else:
        input_shape = (img_height, img_width, 3)    # Tensorflow backend

    # Model producing embedding vector for a single image
    input_data = Input(shape=input_shape)

    # Data sources
    train_dir = os.path.join(args['dataset_dir'], 'images', 'train')
    validation_dir = os.path.join(args['dataset_dir'], 'images', 'val')
    num_train_classes = len(os.listdir(train_dir))
    num_val_classes = len(os.listdir(validation_dir))
    assert(num_val_classes == num_train_classes)

    train_datagen = ImageDataGenerator(
        samplewise_center=True,
        samplewise_std_normalization=True,
        horizontal_flip=True)

    validation_datagen = ImageDataGenerator(
        samplewise_center=True,
        samplewise_std_normalization=True)

    train_generator = train_datagen.flow_from_directory(
        train_dir,
        target_size=(img_height, img_width),
        batch_size=args['batch_size'],
        class_mode='sparse')
    validation_generator = validation_datagen.flow_from_directory(
        validation_dir,
        target_size=(img_height, img_width),
        batch_size=args['batch_size'],
        class_mode='sparse')
    
    # Create model
    if args['mode'] == 'from_scratch':
        net = models.get_model(args['model_arch'],  input_data)
        net = Dense(num_train_classes, activation='softmax') (net)
        model = Model(inputs=input_data, outputs=net)
    else:
        files = os.listdir(args['pretrained_model_dir'])
        for f in files:
            if f[0] == 'e':
                model = load_model(os.path.join(args['pretrained_model_dir'], f))

    model.compile(loss='sparse_categorical_crossentropy', optimizer=SGD(lr=0.01, momentum=0.9), metrics=['acc'])
    #model.compile(loss='sparse_categorical_crossentropy', optimizer=Adam(lr=0.002), metrics=['acc'])
    model.summary()
    
    # Training monitoring callback
    pretrained_model_dir = None
    if args['mode'] == 'from_pretrained':
        pretrained_model_dir = args['pretrained_model_dir']

    training_cb = train_common.TrainingCallback(model, args['save_model_dir'], pretrained_model_dir)
    reduce_lr_cb = keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=10, verbose=0, mode='auto', epsilon=0.0001, cooldown=0, min_lr=0)
    model.fit_generator(
        train_generator,
        steps_per_epoch=None,
        epochs=150,
        validation_data=validation_generator,
        validation_steps=None,
        callbacks=[training_cb, reduce_lr_cb])
