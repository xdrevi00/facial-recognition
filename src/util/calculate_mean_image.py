import sys
import os
import argparse
import helper
import numpy as np
import cv2

''' Calculates the mean image for a dataset. Input directory should have structure

    input_dir/
        class1/
            image1.ext
            image2.ext
            ...
        class2/
            ...
'''

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('images_dir', type=str, help="Input image directory.")
    parser.add_argument('--mean_image_path', type=str, default='mean_bgr_image', help="Mean image path.")
    parser.add_argument('--display', action='store_true', default=False, help="Displays the mean image.")
    args = parser.parse_args()
    return vars(args)

def write_image(file_name, image):
    if os.path.isfile(file_name):
        print('File {} exists already. Creating backup.'.format(file_name))
        os.rename(file_name, file_name + '.bak')
    np.save(file_name, image)

def calculate_mean_image(args):
    images = helper.get_all_images_in_directory(args['images_dir'])

    mean_image = None
    for image in images:
        image_bgr = image.get_bgr()
        if mean_image is None:
            mean_image = image_bgr.astype('float32')
        else:
            if mean_image.shape != image_bgr.shape:
                Exception('Image dimensions {} do not match with the mean image dimensions {}.'.format(image_bgr.shape, mean_image.shape))
            mean_image += image_bgr.astype('float32')
    
    mean_image /= float(len(images))
    write_image(args['mean_image_path'], mean_image)
    if args['display']:
        cv2.imshow('mean image', mean_image.astype('uint8'))
        cv2.waitKey(0)
        cv2.destroyAllWindows()
    
if __name__ == '__main__':
    args = parse_args()
    calculate_mean_image(args)