import sys
import os
import argparse
import cv2
from align_dlib import AlignDlib
from concurrent.futures import ThreadPoolExecutor

# Generate aligned images for the dataset

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('input_dir', help="Input image directory.")
    parser.add_argument('output_dir', help="Directory for aligned images.")
    parser.add_argument('--aligned_size', default=96, help="Aligned image size.")
    parser.add_argument('--verbose', action='store_true', help="Verbose processing.")
    proj_dir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
    dlib_predictor_path = os.path.join(proj_dir, 'models/dlib/shape_predictor_68_face_landmarks.dat')
    parser.add_argument('--dlib_face_predictor', default=dlib_predictor_path, help="dlib face predictor path.")
    args = parser.parse_args()
    return vars(args)

def load_rgb(image_path):
    try:
        bgr = cv2.imread(image_path)
    except:
        bgr = None
    if bgr is not None:
        try:
            rgb = cv2.cvtColor(bgr, cv2.COLOR_BGR2RGB)
        except:
            rgb = None
    else:
        rgb = bgr
    return rgb

def align_class_dir(class_dir, dlib_aligner, args):
    original_class_dir = os.path.join(args['input_dir'], class_dir)
    aligned_class_dir = os.path.join(args['output_dir'], class_dir)

    original_images = os.listdir(original_class_dir)
    for image in original_images:
        # Read original image and align it if possible
        original_path = os.path.join(original_class_dir, image)
        rgb = load_rgb(original_path)
        if rgb is None:
            out_rgb = None
            if args['verbose']:
                print ('Cannot align ', original_path)
        else:
            out_rgb = dlib_aligner.align(args['aligned_size'], rgb)
            if out_rgb is None and args['verbose']:
                print ('Cannot align ', original_path)
        
        # Write aligned image
        if out_rgb is not None:
            if not os.path.exists(aligned_class_dir):
                os.makedirs(aligned_class_dir)

            out_bgr = cv2.cvtColor(out_rgb, cv2.COLOR_RGB2BGR)
            aligned_path = os.path.join(aligned_class_dir, image)
            cv2.imwrite(aligned_path, out_bgr)
            if args['verbose']:
                print('Writing ', aligned_path)

def generate_aligned_images(args):
    if not os.path.exists(args['output_dir']):
        os.makedirs(args['output_dir'])

    # Single thread per class directory
    with ThreadPoolExecutor() as executor:
        dlib_aligner = AlignDlib(args['dlib_face_predictor'])
        classes = next(os.walk(args['input_dir']))[1]
        for class_dir in classes:
            executor.submit(align_class_dir, class_dir, dlib_aligner, args)

if __name__ == '__main__':
    args = parse_args()
    generate_aligned_images(args)