import os
import numpy as np
from keras.models import load_model
from util.image import Image

def get_all_images_in_directory(directory):
    images = []
    for subdir, dirs, files in os.walk(directory):
        for image_name in files:
            image_class = os.path.basename(subdir)
            image_path = os.path.join(directory, image_class, image_name)
            images.append(Image(image_path))

    images.sort(key = lambda image: image.path)   # Sort by path
    return images
    
def get_classes_to_labels(label_file):
    classes_to_labels = {}
    with open(label_file) as f:
        for line in f:
            class_name, label = line.strip().split()
            classes_to_labels[class_name] = int(label)
    return classes_to_labels

def get_labels_to_classes(label_file):
    labels_to_classes = {}
    with open(label_file) as f:
        for line in f:
            class_name, label = line.strip().split()
            labels_to_classes[int(label)] = class_name
    return labels_to_classes

def images_to_np_arrays(images, names_to_labels, train_set_mean_image=None):
    n = len(images)
    w, h, ch = images[0].get_bgr().shape
    X = np.zeros((n, w, h, ch), dtype=np.float32)
    y = np.zeros((n, 1), dtype=np.float32)
    for i, img in enumerate(images):
        X[i] = img.get_bgr().astype('float32')
        if train_set_mean_image is not None:
            X[i] -= train_set_mean_image
        y[i] = names_to_labels[img.cls]
    return X, y

'''
Expected structure:
data_dir/
    images/
        train/
            person_1/
                person_1_001.jpg
                person_1_002.jpg
                ...
            person_2/
                person_2_001.jpg
                ...
        val/
            person_1/
                person_1_001.jpg
                person_1_002.jpg
                ...
            person_2/
                person_2_001.jpg
'''            
def get_dataset(data_dir, featurewise_centering=False):
    names_to_labels = get_classes_to_labels(os.path.join(data_dir, 'labels.txt'))
    train_subdir = os.path.join(data_dir, 'images', 'train')
    val_subdir = os.path.join(data_dir, 'images', 'val')
    if featurewise_centering:
        train_set_mean_image = np.load(os.path.join(data_dir, 'mean_train_image.npy'))
    else:
        train_set_mean_image = None

    val_images = get_all_images_in_directory(val_subdir)
    train_images = get_all_images_in_directory(train_subdir)

    X_train, y_train = images_to_np_arrays(train_images, names_to_labels, train_set_mean_image)
    X_val, y_val = images_to_np_arrays(val_images, names_to_labels, train_set_mean_image)
    num_classes = len(names_to_labels)
    return X_train, y_train, X_val, y_val, num_classes

# Recursively counts the number of files in a directory
def get_num_files_in_directory(dir):
    total = 0
    for root, dirs, files in os.walk(dir):
        total += len(files)
    return total

def get_dataset_image_count(data_dir):
    train_subdir = os.path.join(data_dir, 'images', 'train')
    val_subdir = os.path.join(data_dir, 'images', 'val')
    
    n_train = get_num_files_in_directory(train_subdir)
    n_val = get_num_files_in_directory(val_subdir)
    return n_train, n_val
    

def collage(data):
    if type(data) is not list:
        if data.shape[3] != 3:
            data = data.transpose(0, 2, 3, 1)
            
        images = [img for img in data]
    else:
        images = list(data)

    side = int(np.ceil(len(images)**0.5))
    for i in range(side**2 - len(images)):
        images.append(images[-1])
    collage = [np.concatenate(images[i::side], axis=0)
               for i in range(side)]
    collage = np.concatenate(collage, axis=1)
    #collage -= collage.min()
    #collage = collage / np.absolute(collage).max() * 256
    return collage