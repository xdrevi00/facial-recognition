import argparse
import errno
import os
import random
import shutil
from collections import defaultdict

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('input_dir', type=str, help="Directory of images to partition in-place to 'train' and 'val' directories.")
    parser.add_argument('--val_ratio', type=float, default=0.10, help="Validation to training ratio.")
    args = parser.parse_args()
    return vars(args)

def create_train_val_split(args, min_val_image_count=2):
    # Put a single image from each class to the validation images, rest to train images
    train_images = []
    val_images = []
    class_val_image_count = defaultdict(int)
    for subdir, _, files in os.walk(args['input_dir']):
        for image in files:
            image_class, image_name = (os.path.basename(subdir), image)
            if class_val_image_count[image_class] < min_val_image_count:
                val_images.append((image_class, image_name))
                class_val_image_count[image_class] += 1
            else:
                train_images.append((image_class, image_name))
    
    # Adjust the number of validation vs train images based on the desired ratio
    current_ratio = float(len(val_images)) / float(len(train_images) + len(val_images))
    #assert(current_ratio < args['val_ratio']) # Ratio is too small
    index = int((len(train_images) + len(val_images)) * args['val_ratio']) - len(val_images)
    random.shuffle(train_images)
    val_images = val_images + train_images[:index]
    train_images = train_images[index:]

    os.makedirs(os.path.join(args['input_dir'], 'train'))
    os.makedirs(os.path.join(args['input_dir'], 'val'))

    for image_class, image_name in train_images:
        new_class_dir = os.path.join(args['input_dir'], 'train', image_class)
        if not os.path.exists(new_class_dir):
            os.makedirs(new_class_dir)
        orig_path = os.path.join(args['input_dir'], image_class, image_name)
        new_path = os.path.join(new_class_dir, image_name)
        shutil.move(orig_path, new_path)

    for image_class, image_name in val_images:
        new_class_dir = os.path.join(args['input_dir'], 'val', image_class)
        if not os.path.exists(new_class_dir):
            os.makedirs(new_class_dir)
        orig_path = os.path.join(args['input_dir'], image_class, image_name)
        new_path = os.path.join(new_class_dir, image_name)
        shutil.move(orig_path, new_path)

    # Delete empty dirs
    for image_class, value in class_val_image_count.items():
        dir_path = os.path.join(args['input_dir'], image_class)
        if os.path.isdir(dir_path):
            os.rmdir(dir_path)

if __name__ == '__main__':
    args = parse_args()
    create_train_val_split(args)