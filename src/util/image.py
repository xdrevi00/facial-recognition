import os
import cv2

class Image:
    def __init__(self, image_path):
        path_components = os.path.abspath(image_path).split('/')
        self.name = path_components[-1]
        self.cls = path_components[-2]
        self.path = image_path

    def get_bgr(self):
        try:
            bgr = cv2.imread(self.path)
        except:
            bgr = None
        return bgr

    def get_rgb(self):
        bgr = self.get_bgr()
        if bgr:
            rgb = cv2.cvtColor(bgr, cv2.COLOR_BGR2RGB)
        else:
            rgb = None
        return rgb