import sys
import os
import argparse
import helper

# Generates person name to class label (int) mapping file from an image directory

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('images_dir', type=str, help="Input image directory.")
    parser.add_argument('label_file', type=str, help="Name to label file.")
    args = parser.parse_args()
    return vars(args)

def generate_labels(images_dir, label_file):
    images = helper.get_all_images_in_directory(images_dir)

    class_label = 0
    classes_written = set()
    with open(label_file, 'w') as f:
        for image in images:
            if image.cls not in classes_written:
                f.write('{} {}\n'.format(image.cls, class_label))
                classes_written.add(image.cls)
                class_label += 1

if __name__ == '__main__':
    args = parse_args()
    generate_labels(args['images_dir'], args['label_file'])