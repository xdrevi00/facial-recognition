import argparse
import keras
import numpy as np
from util import helper

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('model', type=str, help="Model used to classify an image.")
    parser.add_argument('image', type=str, help="Image to be classified.")
    parser.add_argument('labels', type=str, help="Class labels file.")
    parser.add_argument('train_mean_image', default=None, type=str, help="Mean image calculated over the training dataset. Has to be subtracted from the evaluated image.")
    args = parser.parse_args()
    return vars(args)

if __name__ == '__main__':
    args = parse_args()
    
    classes_to_labels = helper.get_classes_to_labels(args['labels'])
    labels_to_classes = helper.get_labels_to_classes(args['labels'])
    img = helper.Image(args['image'])
    x = img.get_bgr().astype('float32')

    if args['train_mean_image'] is not None:
        x -= np.load(args['train_mean_image'])
    X = np.zeros((1, *x.shape))
    X[0] = x
    y = classes_to_labels[img.cls]

    model = keras.models.load_model(args['model'])
    y_pred = model.predict(X)
    print(y_pred)
    most_probable_label = np.argmax(y_pred[0])

    print('Testing file {} Correct class: {}, Model prediction: {}'.format(args['image'], img.cls, labels_to_classes[most_probable_label]))

