import argparse
import os
import cv2


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('lfw_deepfunneled', type=str, help="Directory that contains LFW deepfunneled dataset.")
    parser.add_argument('lfw_dlib', type=str, help="Directory that contains LFW dataset aligned with dlib.")
    parser.add_argument('image_size', type=int, default=96,
                        help="Text file with paired images. See http://vis-www.cs.umass.edu/lfw/pairs.txt")
    args = parser.parse_args()
    return vars(args)


if __name__ == "__main__":
    args = parse_args()

    lfw_deepfunnel = args['lfw_deepfunneled']
    lfw_dlib = args['lfw_dlib']
    img_size = int(args['image_size'])

    for person_name in os.listdir(lfw_deepfunnel):
        for img in os.listdir(os.path.join(lfw_deepfunnel, person_name)):
            img_dlib = os.path.join(lfw_dlib, person_name, img)
            if not os.path.exists(img_dlib):
                print(img)
                image = cv2.imread(os.path.join(lfw_deepfunnel, person_name, img))
                size = image.shape
                image = image[int(size[0] / 2) - int(img_size / 2):int(size[0] / 2) + int(img_size / 2),
                        int(size[1] / 2) - int(img_size / 2):int(size[1] / 2) + int(img_size / 2)]

                person_dir = os.path.join(lfw_dlib, person_name)
                if not os.path.exists(person_dir):
                    os.makedirs(person_dir)

                cv2.imwrite(os.path.join(person_dir, img), image)
