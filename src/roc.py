import os
import argparse
import sys
import evaluate_lfw as eval
import re
import json
import numpy as np
import matplotlib.pyplot as plt
import datetime

EVAL_FILE_PATTERN = "eval_file_.*"

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('eval_dir', type=str, default="./", help="Directory that contains eval files.")
    args = parser.parse_args()
    return vars(args)

def main():
    args = parse_args()
    dir = args['eval_dir']
    if not os.path.exists(dir):
        print("The "+dir+" does not exist.")
        sys.exit(-1)

    acc = 0
    auc = 0
    tp_rate = []
    fp_rate = []
    name = ""

    m = re.compile(EVAL_FILE_PATTERN)
    for file in os.listdir(dir):
        if m.match(file):
            with open(os.path.join(dir,file)) as f:
                content = f.read()
                dic = json.loads(content)
                acc = dic[eval.ACCURACY_ID]
                auc = dic[eval.AUC_ID]
                tp_rate = np.array(dic[eval.TP_RATE_ID])
                fp_rate = np.array(dic[eval.FP_RATE_ID])
                name = dic[eval.NAME_ID]

            plt.plot(fp_rate, tp_rate, label=name+' (AUC = %0.3f, acc = %0.3f)' % (auc, acc))

    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('ROC')
    plt.legend(loc="lower right")
    plt.grid(True)
    plt.savefig('plot_'+datetime.datetime.now().isoformat()+'.jpeg')
    plt.show()

if __name__=="__main__":
    main()
