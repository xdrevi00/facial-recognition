from models import models
from keras.layers import Input
from keras.models import Model
from IPython.display import SVG
from keras.utils.vis_utils import model_to_dot
from keras.utils import plot_model

input_data = Input(shape=(96,96,3))
m = models.vgg_16(input_data)

model = Model(input_data, m)
model.summary()

plot_model(model, to_file='model.png')
SVG(model_to_dot(model).create(prog='dot', format='svg'))
