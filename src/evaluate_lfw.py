# Project POVa
# Facial recognition
# Authors: Dusan Drevicky, Michal Klco, Filip Badan
import argparse
import os
import sys
import keras
import numpy as np
from models import models
import gc
import train_common as tc
from train_common import contrastive_loss, accuracy_for_contrastive_loss
from keras import backend as K
from keras.models import Model
from keras.layers import Input, Dense, Lambda
from PIL import Image as pil_image
import matplotlib.pyplot as plt
import json

ACCURACY_ID = 'acc'
AUC_ID = 'auc'
FP_RATE_ID = 'fp_rate'
TP_RATE_ID = 'tp_rate'
NAME_ID = 'name'

class Fold(tc.PairDataset):
    def __init__(self, positive_img_paths, negative_img_paths):
        self.positive_pairs = positive_img_paths
        self.negative_pairs = negative_img_paths

    def _pair_generator(self, img_paths):
        for path1, path2 in img_paths:
            img1 = pil_image.open(path1)
            img2 = pil_image.open(path2)
            if img1.mode != 'RGB':
                img1 = img1.convert('RGB')
            if img2.mode != 'RGB':
                img2 = img2.convert('RGB')

            img1 = self._preprocess(np.asarray(img1, K.floatx()))
            img2 = self._preprocess(np.asarray(img2, K.floatx()))

            yield (img1, img2)

    def get_positive_pair_generator(self):
        return self._pair_generator(self.positive_pairs)

    def get_negative_pair_generator(self):
        return self._pair_generator(self.negative_pairs)

class LFWPairDataset(object):
    def __init__(self, dataset_dir, pairs_file):
        num_of_folds = 10
        num_of_pairs = 300
        same = []
        different = []
        count = 0
        self.folds = []

        print ('Loading LFW dataset.')
        with open(pairs_file) as fp:
            for line in fp:
                line_split = line.split()

                if len(line_split) == 2:
                    num_of_folds = int(line_split[0])
                    num_of_pairs = int(line_split[1])
                elif len(line_split) == 3:
                    # images with the same identity
                    img1 = self._get_image_file(line_split[0], int(line_split[1]))
                    img2 = self._get_image_file(line_split[0], int(line_split[2]))
                    same.append(np.array((os.path.join(dataset_dir, img1), os.path.join(dataset_dir, img2))))
                elif len(line_split) == 4:
                    # images with the different identity
                    img1 = self._get_image_file(line_split[0], int(line_split[1]))
                    img2 = self._get_image_file(line_split[2], int(line_split[3]))
                    different.append(np.array((os.path.join(dataset_dir, img1), os.path.join(dataset_dir, img2))))

                if count == 2 * num_of_pairs:
                    count = 0
                    self.folds.append(Fold(np.array(same), np.array(different)))
                    same = []
                    different = []

                count += 1
        print('LFW loaded.')

    def _get_image_file(self, name, number):
        '''
            Returns filename of the image of the given class and number
        :param name:
        :param number:
        :return:
        '''
        num = '{:04d}'.format(number)
        return os.path.join(name, name + "_" + num + ".jpg")

    def get_all_folds(self):
        return self.folds

    def get_fold(self, index):
        return self.folds[index]

def eval_accuracy(threshold, distances_same, distances_diff):
    '''

    :param threshold:
    :param distances_same: array of embedding distances calculated from faces of the same person
    :param distances_diff: array of embedding distances calculated from faces of the different persons
    :return:
    '''
    return np.mean(np.concatenate((distances_same <= threshold, distances_diff > threshold)))

def get_thresholds():
    return np.arange(0, 2, 0.01)


def get_best_threshold(distances_same, distances_diff):
    thresholds = get_thresholds()
    max = 0
    threshold = thresholds[0]
    for t in thresholds:
        acc = eval_accuracy(t, distances_same, distances_diff)
        if acc > max:
            max = acc
            threshold = t
    return threshold

def get_embedding(model, data):
    out = model.predict(np.array([data]))
    return out

def euclidean_distance_numpy(xa, xb):
    return np.sqrt(np.sum((xa-xb)**2, axis=1))

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('model_file', type=str, help="Path to Keras model used to classify an image.")
    parser.add_argument('model_name', type=str, help="Model name.")
    parser.add_argument('dataset_dir', type=str, help="Directory that contains LFW dataset.")
    parser.add_argument('pairs', type=str,
                        help="Text file with paired images. See http://vis-www.cs.umass.edu/lfw/pairs.txt")
    args = parser.parse_args()
    return vars(args)


def evaluate(model, model_name, pairs_file, dataset_dir):
    '''
        Evaluate FLW dataset using 10-fold cross validation.
        See http://vis-www.cs.umass.edu/lfw/#views
    :param model:
    :param pairs_file:
    :param dataset_dir:
    :return:
    '''
    lfw = LFWPairDataset(dataset_dir, pairs_file)
    folds = lfw.get_all_folds()
    accuracies = []
    tps = []
    fps = []

    # Prepare all folds
    positive_dist_fold = []
    negative_dist_fold = []
    for i in range(0, len(folds)):
        embed1 = []
        embed2 = []
        for img1, img2 in folds[i].get_positive_pair_generator():
            embed1.append(get_embedding(model, img1))
            embed2.append(get_embedding(model, img2))

        distances_positive = euclidean_distance_numpy(np.array(embed1).squeeze(), np.array(embed2).squeeze())
        embed1.clear()
        embed2.clear()

        for img1, img2 in folds[i].get_negative_pair_generator():
            embed1.append(get_embedding(model, img1))
            embed2.append(get_embedding(model, img2))

        distances_negative = euclidean_distance_numpy(np.array(embed1).squeeze(), np.array(embed2).squeeze())
        embed1.clear()
        embed2.clear()

        positive_dist_fold.append(distances_positive)
        negative_dist_fold.append(distances_negative)

    # KFold cross validation
    for i in range(0, len(folds)):
        # i is the number of validation fold
        train_distances_negative = []
        train_distances_positive = []
        val_distances_positive = []
        val_distances_negative = []

        for j in range(0, len(folds)):
            if i == j:
                # Validation part
                val_distances_negative = negative_dist_fold[j]
                val_distances_positive = positive_dist_fold[j]

            else:
                train_distances_positive.extend(positive_dist_fold[j])
                train_distances_negative.extend(negative_dist_fold[j])

        train_positive = np.array(train_distances_positive)
        train_negative = np.array(train_distances_negative)
        val_positive = np.array(val_distances_positive)
        val_negative = np.array(val_distances_negative)

        threshold = get_best_threshold(train_positive, train_negative)
        acc = eval_accuracy(threshold, val_positive, val_negative)
        tp_rate, fp_rate = get_tp_fp_rates(val_positive, val_negative)

        accuracies.append(acc)
        tps.append(tp_rate)
        fps.append(fp_rate)

    tps_array = np.average(np.array(tps), axis=0)
    fps_array = np.average(np.array(fps), axis=0)

    area_uc = auc(tps_array, fps_array)
    acc =  np.average(np.array(accuracies))
    create_eval_file(model_name, acc, area_uc, tps_array.tolist(), fps_array.tolist())

    return np.average(np.array(accuracies)), area_uc

def get_tp_fp_rates(distances_positive, distances_negative):
    thresholds = get_thresholds()
    tp_rate = []
    fp_rate = []
    for t in thresholds:
        tp_rate.append(np.average(distances_positive <= t))
        fp_rate.append(np.average(distances_negative <= t))
    return np.array(tp_rate), np.array(fp_rate)

def auc(tp_rate, fp_rate):
    return np.trapz(tp_rate, fp_rate)

def create_eval_file(model_name, acc, area_uc, tp_rate, fp_rate):
    '''
        Write json file with parameters of the given model.
    :param model_name:
    :param acc:
    :param area_uc:
    :param tp_rate:
    :param fp_rate:
    :return:
    '''
    d = {}
    d[ACCURACY_ID] = acc
    d[AUC_ID] = area_uc
    d[TP_RATE_ID] = tp_rate
    d[FP_RATE_ID] = fp_rate
    d[NAME_ID] = model_name

    json_content = json.dumps(d)
    file_name = "eval_file_"+model_name
    with open(file_name, 'w') as f:
        f.write(json_content)

def main():
    args = parse_args()
    model_file = args['model_file']
    model_name = args['model_name']
    pairs_file = args['pairs']
    dataset_dir = args['dataset_dir']

    if not os.path.isfile(model_file):
        print("The " + model_file + " file cannot be found.")
        sys.exit(-1)
    if not os.path.isfile(pairs_file):
        print("The " + pairs_file + " file cannot be found.")
        sys.exit(-1)
    if not os.path.exists(dataset_dir):
        print("The " + dataset_dir + " directory cannot be found.")
        sys.exit(-1)

    model = keras.models.load_model(model_file, custom_objects={'contrastive_loss' : contrastive_loss, 'accuracy_for_contrastive_loss' : accuracy_for_contrastive_loss})

    #inp = keras.layers.Input((96, 96, 3))
    #outp = models.get_model('basic', inp)
    #model = keras.models.Model(inp, outp)
    #acc, area_uc = evaluate(model, "pokus2", pairs_file, dataset_dir)
    #print(str(acc))

    acc, auc = evaluate(model, model_name, pairs_file, dataset_dir)
    print("Accuracy on LFW: {}, AUC: {}".format(acc, auc))

if __name__ == "__main__":
    main()
