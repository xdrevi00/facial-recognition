import os
import time
import argparse
import numpy as np
import keras
from keras import backend as K
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Flatten, Activation, Input
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import BatchNormalization
from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import SGD
from keras.callbacks import ModelCheckpoint

from models import models
import train_common

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('dataset_dir', type=str, help="Dataset directory containing images folder and labels.txt file.")
    parser.add_argument('model_dir', type=str, help="""Directory name where the model and training loss history will be saved. Data is saved to 
                                                        project_root/models/trained/model_name directory.""")
    parser.add_argument('--image_dim', type=int, default=96, help="Dimensions of the aligned images in the dataset. Width and height should be the same.")
    parser.add_argument('--batch_size', type=int, default=64, help='Batch size.')

    args = parser.parse_args()
    return vars(args)

# Run like this for example: python src/train.py overfit/ basic_model.
if __name__ == '__main__':
    args = parse_args()
    
    img_width, img_height = args['image_dim'], args['image_dim']
    if K.image_data_format() == 'channels_first':
        input_shape = (3, img_width, img_height)    # Theano backend
    else:
        input_shape = (img_height, img_width, 3)    # Tensorflow backend

    # Data sources
    train_dir = os.path.join(args['dataset_dir'], 'images', 'train')
    validation_dir = os.path.join(args['dataset_dir'], 'images', 'val')
    num_train_classes = len(os.listdir(train_dir))
    num_val_classes = len(os.listdir(validation_dir))
    assert(num_val_classes == num_train_classes)

    train_datagen = ImageDataGenerator(
        samplewise_center=True,
        samplewise_std_normalization=True,
        horizontal_flip=True)

    validation_datagen = ImageDataGenerator(
        samplewise_center=True,
        samplewise_std_normalization=True)

    train_generator = train_datagen.flow_from_directory(
        train_dir,
        target_size=(img_height, img_width),
        batch_size=args['batch_size'],
        class_mode='sparse')
    validation_generator = validation_datagen.flow_from_directory(
        validation_dir,
        target_size=(img_height, img_width),
        batch_size=args['batch_size'],
        class_mode='sparse')
    
    # Create convolution model (without the top dense layers)
    #base_model = keras.applications.vgg16.VGG16(include_top=False, weights='imagenet', input_shape=input_shape, pooling=None)
    base_model = keras.applications.inception_v3.InceptionV3(include_top=False, weights='imagenet', input_tensor=None, input_shape=input_shape, pooling='avg')
    
    # Add classifier on top
    x = base_model.output
    
    output = Dense(num_train_classes, activation='softmax') (x)
    
    # Connect the models
    model = Model(inputs=base_model.input, outputs=output)

    # Freeze VGG layers
    for layer in base_model.layers:
        layer.trainable = False

    # Create model
    sgd = SGD(lr=0.01, decay=1e-4, momentum=0.9, nesterov=True)
    model.compile(loss='sparse_categorical_crossentropy', optimizer=sgd, metrics=['acc'])
    model.summary()

    # Training monitoring callback
    proj_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    trained_models_dir = os.path.join(proj_dir, 'models', 'trained')
    model_dir = os.path.join(trained_models_dir, args['model_dir'])
    training_cb = train_common.TrainingCallback(model, model_dir)
    
    model.fit_generator(
        train_generator,
        steps_per_epoch=None,
        epochs=150,
        validation_data=validation_generator,
        validation_steps=None,
        callbacks=[training_cb])
