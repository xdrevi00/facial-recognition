import os
import time
import numpy as np
import keras
from keras.models import load_model
from keras import backend as K
from PIL import Image as pil_image

class TrainingCallback(keras.callbacks.Callback):
    def __init__(self, model, save_model_dir, existing_model_dir=None, accuracy_function='acc'):
        self.model = model
        self.accuracy_function = accuracy_function
        self.model_dir = save_model_dir
        os.makedirs(self.model_dir)
        if existing_model_dir is not None:
            split_path = existing_model_dir.split('/')
            parent_name = split_path[-1]
            if parent_name == '':
                parent_name = split_path[-2]
            with open(parent_name, "w") as f:
                pass

        self.epoch = 1
        self.model_file = ''
        self.train_batch_loss_file = os.path.join(self.model_dir, 'train_batch_loss.csv')
        self.train_batch_acc_file = os.path.join(self.model_dir, 'train_batch_acc.csv')
        self.train_epoch_loss_file = os.path.join(self.model_dir, 'train_epoch_loss.csv')
        self.train_epoch_acc_file = os.path.join(self.model_dir, 'train_epoch_acc.csv')
        self.val_epoch_loss_file = os.path.join(self.model_dir, 'val_epoch_loss.csv')
        self.val_epoch_acc_file = os.path.join(self.model_dir, 'val_epoch_acc.csv')

        self.best_val_acc = 0.0
    
    def on_batch_end(self, batch, logs={}):
        with open(self.train_batch_loss_file, "a") as f: f.write(str(logs.get('loss')) + '\n')
        with open(self.train_batch_acc_file, "a") as f: f.write(str(logs.get(self.accuracy_function)) + '\n')

    def on_epoch_end(self, end, logs={}):
        with open(self.train_epoch_loss_file, "a") as f: f.write(str(logs.get('loss')) + '\n')
        with open(self.train_epoch_acc_file, "a") as f: f.write(str(logs.get(self.accuracy_function)) + '\n')
        with open(self.val_epoch_loss_file, "a") as f: f.write(str(logs.get('val_loss')) + '\n')
        with open(self.val_epoch_acc_file, "a") as f: f.write(str(logs.get('val_' + self.accuracy_function)) + '\n')

        # Save checkpoint
        current_val_acc = logs.get('val_' + self.accuracy_function)
        if current_val_acc > self.best_val_acc:
            if os.path.exists(self.model_file):
                os.remove(self.model_file)
            model_name = 'e{0}_ta{1:.3f}_va{2:.3f}.h5'.format(self.epoch, logs.get(self.accuracy_function), current_val_acc)
            print ('\nValidation accuracy improved. Saving new checkpoint at {}'.format(model_name))
            self.model_file = os.path.join(self.model_dir, model_name)
            self.model.save(self.model_file)
            self.best_val_acc = current_val_acc
        
        self.epoch += 1

class PairDataset(object):
    def __init__(self, image_dir, image_size=96):
        self.images = []            # list [identities][images_for_identity]
        self.image_shape = (image_size, image_size, 3)
        person_dirs = next(os.walk(image_dir))[1]
        person_dirs.sort()
        print('Loading {} identities.'.format(len(person_dirs)))
        
        for person_dir in person_dirs:
            image_files = os.listdir(os.path.join(image_dir, person_dir))
            image_files.sort()
            tmp = np.zeros((len(image_files), image_size, image_size, 3), dtype=np.uint8)
            for i, image_file in enumerate(image_files):
                img = pil_image.open(os.path.join(image_dir, person_dir, image_file))
                if img.mode != 'RGB':
                    img = img.convert('RGB')
                tmp[i] = np.asarray(img, np.uint8)  # Assuming TF backend, would have to reorder for Theano
            self.images.append(tmp)
        print ('Dataset loaded.')
    
    def _preprocess(self, img):
        img -= np.mean(img, keepdims=True)
        img /= (np.std(img, keepdims=True) + K.epsilon())
        return img

    def _create_positive_pairs(self, num_requested):
        person_ids = np.random.choice(len(self.images), num_requested)

        img_1 = np.zeros((num_requested, *self.image_shape), dtype=K.floatx())
        img_2 = np.zeros((num_requested, *self.image_shape), dtype=K.floatx())
        num_created = 0
        for id in person_ids:
            if len(self.images[id]) < 2:
                continue
            pair = np.random.choice(len(self.images[id]), 2, replace=False)
            img_1[num_created] = self._preprocess(self.images[id][pair[0]].astype(K.floatx()))
            img_2[num_created] = self._preprocess(self.images[id][pair[1]].astype(K.floatx()))
            num_created += 1

        return img_1[:num_created], img_2[:num_created]

    def _create_negative_pairs(self, count):
        img_1 = np.zeros((count, *self.image_shape), dtype=K.floatx())
        img_2 = np.zeros((count, *self.image_shape), dtype=K.floatx())
        for i in range(count):
            pair = np.random.choice(len(self.images), 2, replace=False)
            img_id_1 = np.random.choice(len(self.images[pair[0]]), 1)
            img_id_2 = np.random.choice(len(self.images[pair[1]]), 1)

            img_1[i] = self._preprocess(self.images[pair[0]][img_id_1].astype(K.floatx()))
            img_2[i] = self._preprocess(self.images[pair[1]][img_id_2].astype(K.floatx()))

        return img_1, img_2

    def create_data(self, pos_pair_count, neg_pair_count):
        pos_a, pos_b = self._create_positive_pairs(pos_pair_count)
        neg_a, neg_b = self._create_negative_pairs(neg_pair_count)
        
        input_net_a = np.concatenate([pos_a, neg_a])
        input_net_b = np.concatenate([pos_b, neg_b])
        labels = np.zeros(input_net_a.shape[0], dtype=np.float32)
        labels[:pos_a.shape[0]] = 1.0
        
        return input_net_a, input_net_b, labels

def load_keras_model(model_dir):
    files = os.listdir(model_dir)
    for file_name in files:
        if file_name[0] == 'e':
            model_path = os.path.join(model_dir, file_name)
            model = load_model(model_path, custom_objects={'contrastive_loss' : contrastive_loss, 'accuracy_for_contrastive_loss' : accuracy_for_contrastive_loss})
    return model

def euclidean_distance(vects):
    x, y = vects
    return K.sqrt(K.maximum(K.sum(K.square(x - y), axis=1, keepdims=True), K.epsilon()))

def eucl_dist_output_shape(shapes):
    shape1, shape2 = shapes
    return (shape1[0], 1)

'''
    y: ground truth label
    y = 1 if same person 
    y = 0 otherwise
    l2_dist: distance between vectors predicted for the pair of images
'''
def contrastive_loss(y, l2_dist):
    margin = 1
    return K.mean(y * K.square(l2_dist) + (1 - y) * K.square(K.maximum(margin - l2_dist, 0)))

def compute_accuracy_with_threshold(y_true, y_pred):
    '''Compute classification accuracy with a fixed threshold on distances.
    '''
    pred = y_pred.ravel() < 0.5
    return np.mean(pred == y_true)

def accuracy_for_contrastive_loss(y_true, y_pred):
    '''Compute classification accuracy with a fixed threshold on distances.
    '''
    model_predicts_similar = K.cast(y_pred < 0.5, y_true.dtype)
    return K.mean(K.equal(y_true, model_predicts_similar))

def l2_normalize(x):
    return K.l2_normalize(x, axis=-1)
    
def sum_vectors(vects):
    x, y, z, w = vects
    return x + y + z + w
