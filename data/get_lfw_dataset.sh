# Downloads a shared file from Google Drive, see https://stackoverflow.com/questions/25010369/wget-curl-large-file-from-google-drive
ggID='1pIxDKc0YIuWQGjE749cYPqZkrBP43Fch'
ggURL='https://drive.google.com/uc?export=download'  
#filename="$(curl -sc /tmp/gcokie "${ggURL}&id=${ggID}" | grep -o '="uc-name.*</span>' | sed 's/.*">//;s/<.a> .*//')"
#getcode="$(awk '/_warning_/ {print $NF}' /tmp/gcokie)" 
#curl -Lb /tmp/gcokie "${ggURL}&confirm=${getcode}&id=${ggID}" -o "${filename}" 

filename=lfw_dlib.zip
wget --no-check-certificate "https://docs.google.com/uc?export=download&id=$ggID" -O $filename
unzip $filename
rm -r $filename



