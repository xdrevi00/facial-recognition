# Downloads a shared file from Google Drive, see https://stackoverflow.com/questions/25010369/wget-curl-large-file-from-google-drive
ggID='1nGO-WsacLFj3XGMK82G8BgPZzCSQGkG9'  
ggURL='https://drive.google.com/uc?export=download'  
filename="$(curl -sc /tmp/gcokie "${ggURL}&id=${ggID}" | grep -o '="uc-name.*</span>' | sed 's/.*">//;s/<.a> .*//')"
getcode="$(awk '/_warning_/ {print $NF}' /tmp/gcokie)" 
curl -Lb /tmp/gcokie "${ggURL}&confirm=${getcode}&id=${ggID}" -o "${filename}" 