Authors: 	Dušan Drevický, xdrevi00
		Michal Klčo, xklcom00
		Filip Badáň, xbadan00

Project: 	Facial Recognition from Images

Description:
This project implements facial recognition from images using convolutional neural networks.

To evaluate our model proceed as follows:
1. Download the VGG CE model using the models/get_vgg_ce_4096.sh script.
2. Download the LFW dataset using the data/get_lfw_dataset.sh script.
3. Run the evaluation using python src/evaluate_lfw.py models/vgg_ce_4096.h5 "VGG_CE_4096" data/lfw_dlib data/lfw_pairs.txt
